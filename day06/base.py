import copy
import functools
import itertools
import math
import re
import string
import sys
from collections import defaultdict
from dataclasses import dataclass
from typing import Optional


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def parse_line(line: str) -> tuple[str, str, str]:
    line = line.removeprefix("turn ")
    match = re.match(r"^(\w+) (\d+,\d+) through (\d+,\d+)$", line)
    assert match is not None
    matches = match.groups()
    return (matches[0], matches[1], matches[2])


def do_one_light(
    lights: dict[tuple[int, int], bool],
    action: str,
    x: int,
    y: int,
) -> None:
    if action == "toggle":
        lights[x, y] = not lights[x, y]
    elif action == "on":
        lights[x, y] = True
    elif action == "off":
        lights[x, y] = False
    else:
        raise RuntimeError(f"unexpected action `{action}`")


def do_one_other_light(
    lights: dict[tuple[int, int], int],
    action: str,
    x: int,
    y: int,
) -> None:
    if action == "toggle":
        lights[x, y] += 2
    elif action == "on":
        lights[x, y] += 1
    elif action == "off":
        lights[x, y] = max(0, lights[x, y] - 1)
    else:
        raise RuntimeError(f"unexpected action `{action}`")


def do_the_lights(
    lights: dict[tuple[int, int], bool],
    action: str,
    startx: int,
    starty: int,
    endx: int,
    endy: int,
) -> None:
    for xrunner in range(startx, endx + 1):
        for yrunner in range(starty, endy + 1):
            do_one_light(lights, action, xrunner, yrunner)


def do_the_other_lights(
    lights: dict[tuple[int, int], int],
    action: str,
    startx: int,
    starty: int,
    endx: int,
    endy: int,
) -> None:
    for xrunner in range(startx, endx + 1):
        for yrunner in range(starty, endy + 1):
            do_one_other_light(lights, action, xrunner, yrunner)


def main() -> None:
    lines = readFile(sys.argv[1])

    lights: dict[tuple[int, int], bool] = defaultdict(lambda: False)
    other_lights: dict[tuple[int, int], bool] = defaultdict(lambda: 0)
    for line in lines:
        action, start, end = parse_line(line)
        startx, starty = [int(num) for num in start.split(",")]
        endx, endy = [int(num) for num in end.split(",")]
        do_the_lights(lights, action, startx, starty, endx, endy)
        do_the_other_lights(other_lights, action, startx, starty, endx, endy)

    print("part1")
    print(len([1 for x, y in itertools.product(
        range(1000), range(1000)) if lights[x, y]]))
    print("part2")
    print(sum([other_lights[x, y]
               for x, y in itertools.product(range(1000), range(1000))]))


if __name__ == "__main__":
    import doctest

    doctest.testmod()

    main()
