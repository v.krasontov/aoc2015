import copy
import doctest
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum
from typing import Callable, Optional, overload

import numpy as np

Position = tuple[int, int]


@dataclass
class Grid:
    layout: dict[Position, bool]
    max_row: int
    max_column: int

    @property
    def corners(self) -> list[Position]:
        return [(0, 0), (0, self.max_column - 1), (self.max_row - 1, 0),
                (self.max_row - 1, self.max_column - 1)]


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def parse_grid(lines: list[str]) -> Grid:
    layout: dict[Position, bool] = {}
    for row, line in enumerate(lines):
        for column, char in enumerate(list(line)):
            layout[(row, column)] = True if char == "#" else False
    grid = Grid(layout, len(lines), len(lines[0]))
    return grid


def get_all_neighbours(current: Position, grid: Grid) -> list[Position]:
    x, y = current
    neighbours = [
        (x - 1, y - 1),
        (x - 1, y),
        (x - 1, y + 1),
        (x, y - 1),
        (x, y + 1),
        (x + 1, y - 1),
        (x + 1, y),
        (x + 1, y + 1),
    ]
    neighbours_in_layout = [(a, b) for a, b in neighbours if 0 <=
                            a < grid.max_row and 0 <= b < grid.max_column]
    return neighbours_in_layout


def light_new_state(current: Position, grid: Grid) -> bool:
    neighbors = get_all_neighbours(current, grid)
    neighbour_states = [grid.layout[neighbour] for neighbour in neighbors]
    neighbours_on = neighbour_states.count(True)
    if grid.layout[current]:
        # A light which is on stays on when 2 or 3 neighbors are on, and turns
        # off otherwise.
        return neighbours_on in [2, 3]
    else:
        # A light which is off turns on if exactly three neighbors are on, and
        # stays off otherwise.
        return neighbours_on == 3


def light_new_state_two(current: Position, grid: Grid) -> bool:
    if current in grid.corners:
        return True
    neighbors = get_all_neighbours(current, grid)
    neighbour_states = [grid.layout[neighbour] for neighbour in neighbors]
    neighbours_on = neighbour_states.count(True)
    if grid.layout[current]:
        # A light which is on stays on when 2 or 3 neighbors are on, and turns
        # off otherwise.
        return neighbours_on in [2, 3]
    else:
        # A light which is off turns on if exactly three neighbors are on, and
        # stays off otherwise.
        return neighbours_on == 3


def how_many_are_on(grid: Grid) -> int:
    return list(grid.layout.values()).count(True)


def do_part_one(lines: list[str]) -> int:
    grid = parse_grid(lines)
    count = 100
    for _ in range(count):
        grid.layout = {pos: light_new_state(pos, grid) for pos in grid.layout}
    return how_many_are_on(grid)


def do_part_two(lines: list[str]) -> int:
    grid = parse_grid(lines)
    for corner in grid.corners:
        grid.layout[corner] = True
    count = 100
    for _ in range(count):
        grid.layout = {
            pos: light_new_state_two(
                pos, grid) for pos in grid.layout}
    return how_many_are_on(grid)


def main(input_file: str) -> None:
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    result_part_one = do_part_one(lines)
    print(result_part_one)
    print("done with part one")

    print()
    print("part2")
    result_part_two = do_part_two(lines)
    print(result_part_two)
    print("done with part two")


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("done with test")

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
