import copy
import sys
import math
from dataclasses import dataclass
from itertools import groupby
from typing import Optional
from functools import reduce, cmp_to_key


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def surface(l, w, h):
    side1 = l * w
    side2 = w * h
    side3 = h * l
    return 2 * sum([side1, side2, side3]) + min([side1, side2, side3])


def ribbon(l, w, h):
    volume = l * w * h
    perimeter = 2 * sum(sorted([l, w, h])[:2])
    return volume + perimeter


def main():
    print("part1")
    lines = readFile(sys.argv[1])
    dims = [[int(num) for num in line.split("x")] for line in lines]
    print(sum([surface(*dim) for dim in dims]))
    print("part2")
    print(ribbon(1, 1, 10))
    print(sum([ribbon(*dim) for dim in dims]))


if __name__ == "__main__":
    main()
