import copy
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass
from typing import Optional


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def next(seq: str) -> str:
    """do_something is an example for a function
    >>> next("1")
    '11'
    >>> next("11")
    '21'
    >>> next("21")
    '1211'
    >>> next("1211")
    '111221'
    >>> next("111221")
    '312211'
    """
    regex = re.compile("|".join(f"{digit}+" for digit in range(10)))
    # print(regex)
    matches = regex.findall(seq)
    new_seq = ""
    for match in matches:
        assert isinstance(match, str)
        digit = match[0]
        length = len(match)
        # print(f"{match=}")
        # print(f"{digit=}")
        # print(f"{match=}")
        new_seq += str(length) + digit
    return new_seq


def main(input_file: str):
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    sequence = lines[0]
    repetitions = 40
    for _ in range(repetitions):
        sequence = next(sequence)
    print(len(sequence))
    print("part2")
    more_reps = 10
    for _ in range(more_reps):
        sequence = next(sequence)
    print(len(sequence))


if __name__ == "__main__":
    import doctest

    doctest.testmod()

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
