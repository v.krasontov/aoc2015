import copy
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict
from dataclasses import dataclass
from typing import Optional


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


# gates:
# AND OR LSHIFT RSHIFT NOT

""" 123 -> x means that the signal 123 is provided to wire x.
    x AND y -> z means that the bitwise AND of wire x and wire y is provided to wire z.
    p LSHIFT 2 -> q means that the value from wire p is left-shifted by 2 and then
    provided to wire q.
    NOT e -> f means that the bitwise complement of the value from wire e is provided to
    wire f.
"""


@dataclass
class Input:
    value: Optional[int] = None
    operation: Optional[str] = None
    one_wire: Optional[str] = None
    other_wire: Optional[str] = None
    shift_value: Optional[int] = None
    calculated_value: Optional[int] = None


def parse_line(inputs: dict[str, Input], line: str) -> None:  # TODO
    words = line.split(" ")
    wire = words[-1]
    if line.startswith("NOT"):
        inputs[wire] = Input(operation="NOT", one_wire=words[1])
        return
    if len(words) == 3:  # direct value
        input_source = words[0]
        try:
            input_value = int(input_source)
            inputs[wire] = Input(value=input_value)
        except ValueError:
            inputs[wire] = Input(one_wire=input_source)
        return
    operation = words[1]
    if operation.endswith("SHIFT"):
        inputs[wire] = Input(
            operation=operation, shift_value=int(words[2]), one_wire=words[0]
        )
        return
    inputs[wire] = Input(operation=operation, one_wire=words[0], other_wire=words[2])
    return


def p(msg):
    if debug:
        print(msg)


def find_value(inputs: dict[str, Input], wire: str):
    p(f"looking at wire (or value) {wire}")
    try:
        direct_value = int(wire)  # we were given a direct value as input
        return direct_value
    except ValueError:
        pass
    inputt = inputs[wire]
    if inputt.calculated_value is not None:
        return inputt.calculated_value

    if inputt.value is not None:
        inputt.calculated_value = inputt.value
    elif inputt.one_wire and inputt.other_wire is None and inputt.operation is None:
        inputt.calculated_value = find_value(inputs, inputt.one_wire)
    elif inputt.operation == "NOT":
        assert inputt.one_wire is not None
        inputt.calculated_value = 65535 - find_value(inputs, inputt.one_wire)
    elif inputt.operation == "AND":
        assert inputt.one_wire is not None
        assert inputt.other_wire is not None
        inputt.calculated_value = find_value(inputs, inputt.one_wire) & find_value(
            inputs, inputt.other_wire
        )
    elif inputt.operation == "OR":
        assert inputt.one_wire is not None
        assert inputt.other_wire is not None
        inputt.calculated_value = find_value(inputs, inputt.one_wire) | find_value(
            inputs, inputt.other_wire
        )
    elif inputt.operation == "LSHIFT":
        assert inputt.shift_value is not None
        assert inputt.one_wire is not None
        inputt.calculated_value = (
            find_value(inputs, inputt.one_wire) << inputt.shift_value
        )
    elif inputt.operation == "RSHIFT":
        assert inputt.shift_value is not None
        assert inputt.one_wire is not None
        inputt.calculated_value = (
            find_value(inputs, inputt.one_wire) >> inputt.shift_value
        )
    else:
        raise RuntimeError("not implemented yet")
    return inputt.calculated_value


def main():
    wire = "a"
    print("part1")
    lines = readFile(sys.argv[1])
    inputs: dict[str, Input] = {}
    for line in lines:
        parse_line(inputs, line)

    value = find_value(inputs, wire)
    print(value)

    print("part2")
    result_part1 = 16076
    inputs: dict[str, Input] = {}
    for line in lines:
        parse_line(inputs, line)
    inputs["b"].value = result_part1
    value = find_value(inputs, wire)
    print(value)


if __name__ == "__main__":
    debug = True
    main()
