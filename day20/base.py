import copy
import doctest
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum
from typing import Callable, Optional, overload

import numpy as np


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def do_the_sieve(max_house: int = 1000000) -> list[int]:
    factor = 10
    presents_for_houses: list[int] = [0] * (max_house + 1)
    for house_index in range(1, max_house + 1):
        presents = 0
        root = math.ceil(math.sqrt(house_index))
        for elf in range(1, root):
            if house_index % elf == 0:
                presents += factor * elf
                presents += factor * (house_index // elf)
        if math.sqrt(house_index) == root:
            presents += factor * root
        presents_for_houses[house_index] = presents
    return presents_for_houses


def do_the_sieve_smaller(max_house: int = 1000000) -> list[int]:
    factor = 11
    presents_for_houses: list[int] = [0] * (max_house + 1)
    for house_index in range(1, max_house + 1):
        presents = 0
        for elf in range(1, 50 + 1):
            if house_index % elf == 0:
                presents += factor * (house_index // elf)
        presents_for_houses[house_index] = presents
    return presents_for_houses


def do_part_one(lines: list[str]) -> int:
    # What is the lowest house number of the house to get at least as many
    # presents as the number in your puzzle input?
    min_number_of_presents = int(lines[0])
    presents_for_houses = do_the_sieve()
    return [
        house_index
        for house_index, presents in enumerate(presents_for_houses)
        if presents >= min_number_of_presents
    ][0]


def do_part_two(lines: list[str]) -> int:
    min_number_of_presents = int(lines[0])
    presents_for_houses = do_the_sieve_smaller()
    return [
        house_index
        for house_index, presents in enumerate(presents_for_houses)
        if presents >= min_number_of_presents
    ][0]


def main(input_file: str) -> None:
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    result_part_one = do_part_one(lines)
    print(result_part_one)
    print("done with part one")

    print()
    print("part2")
    result_part_two = do_part_two(lines)
    print(result_part_two)
    print("done with part two")


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("doctests were successful!")

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
