import copy
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict
from dataclasses import dataclass
from typing import Optional


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def do_something(x: int) -> bool:
    """do_something is an example for a function
    >>> do_something(4)
    False
    >>> do_something(5)
    True
    """
    return bool(x % 2)


# his elves have provided him the distances between every pair of locations

# He can start and end at any two (different) locations he wants
# but he must visit each location exactly once


@dataclass
class Neighbour:
    name: str
    distance: int

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return f"({self.name} - {self.distance})"


@dataclass
class Graph:
    name: str
    neighbours: list[Neighbour]


def build_graph(lines: list[str]) -> dict[str, list[Neighbour]]:
    graph: dict[str, list[Neighbour]] = {}
    for line in lines:
        source, _, target, _, distance_str = line.split(" ")
        if source not in graph:
            graph[source] = []
        if target not in graph:
            graph[target] = []
        graph[source].append(Neighbour(target, int(distance_str)))
        graph[target].append(Neighbour(source, int(distance_str)))
    return graph


def find_cycles(
    graph: dict[str, list[Neighbour]],
    current_node: str,
    cycle: list[str],
    cycle_weight: int,
) -> list[tuple[list[str], int]]:
    print(f"looking at node {current_node}, path so far: {cycle}")
    if len(cycle) == len(graph):
        return [(cycle, cycle_weight)]
    all_cycles: list[tuple[list[str], int]] = []
    for neighbour in graph[current_node]:
        if neighbour.name in cycle:
            continue
        cycle_with_neighbour = copy.copy(cycle)
        cycle_with_neighbour.append(neighbour.name)
        cycle_weight_this_neighbour = cycle_weight + neighbour.distance
        cycles_this_neighbour = find_cycles(
            graph, neighbour.name, cycle_with_neighbour, cycle_weight_this_neighbour
        )
        all_cycles.extend(cycles_this_neighbour)
    return all_cycles


def main(input_file: str):
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    graph = build_graph(lines)
    print(f"number of nodes: {len(graph)}")
    print(f"nodes: {list(graph.keys())}")
    print("graph:")
    all_cycles: list[tuple[list[str], int]] = []
    print("computing cycles...")
    for key in graph:
        # print(f"considering {key}: {graph[key]}")
        cyles_starting_with_key = find_cycles(graph, key, [key], 0)
        print(cyles_starting_with_key)
        all_cycles.extend(cyles_starting_with_key)
    for cycle in all_cycles:
        print(cycle)
    # what is the shortest distance he can travel to achieve this?
    print(f"result: {min(length for _, length in all_cycles)}")
    print()
    print("part2")
    print(f"result: {max(length for _, length in all_cycles)}")


if __name__ == "__main__":
    import doctest

    doctest.testmod()

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
