import copy
import doctest
import functools
import graphviz
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum
from typing import Callable, Optional, overload


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def do_part_one(lines: list[str]) -> int:
    return 0


def parse_line(line: str) -> tuple[str, str, int]:
    """
    >>> parse_line("David would gain 41 happiness units by sitting next to Carol.")
    ('David', 'Carol', 41)
    >>> parse_line("Carol would lose 62 happiness units by sitting next to Alice.")
    ('Carol', 'Alice', -62)
    """
    words = line.split(" ")
    fromm, to, sign, value = (
        words[0],
        words[-1].removesuffix("."),
        1 if words[2] == "gain" else -1,
        int(words[3]),
    )
    return (fromm, to, sign * value)


# TODO make version with cache
# def get_path_calculator


def calculate_path_cost(path: list[str], edges: dict[str, dict[str, int]]) -> int:
    cost = 0
    for runner in range(len(path) - 1):
        fromm = path[runner]
        to = path[runner + 1]
        cost += (
            edges[fromm][to]
            if fromm in edges and to in edges[fromm]
            else edges[to][fromm]
        )
    first, last = path[0], path[-1]
    cost += (
        edges[last][first]
        if last in edges and first in edges[last]
        else edges[first][last]
    )
    return cost


def main(input_file: str) -> None:
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    result_part_one = do_part_one(lines)

    pre_edges = [parse_line(line) for line in lines]

    nodes_with_edges: dict[str, dict[str, int]] = {}
    for fromm, to, cost in pre_edges:
        if to in nodes_with_edges:
            nodes_with_edges[to][fromm] = nodes_with_edges[to].get(fromm, 0) + cost
        else:
            if fromm not in nodes_with_edges:
                nodes_with_edges[fromm] = {}
            nodes_with_edges[fromm][to] = cost

    for node, neighbours in nodes_with_edges.items():
        print(node, neighbours)

    # dot = graphviz.Graph()
    # for node, neighbours in nodes_with_edges.items():
    #     for neighbour, cost in neighbours.items():
    #         dot.edge(node, neighbour, label=f"{cost}")
    # print("about to render...")
    # dot.render("view-it", "/tmp", view=True)

    nodes = []
    for node, _, _ in pre_edges:
        if not node in nodes:
            nodes.append(node)
    first = nodes[0]
    rest = nodes[1:]
    # print(first)
    # print(rest)
    assert len(nodes) == len(rest) + 1

    all_paths = [[first] + list(path) for path in itertools.permutations(rest)]

    path_lengths = [calculate_path_cost(path, nodes_with_edges) for path in all_paths]
    # for i in range(len(all_paths)):
    #     print(all_paths[i])
    #     print(path_lengths[i])
    # print()

    sorted_paths = sorted(
        all_paths, key=(lambda path: calculate_path_cost(path, nodes_with_edges))
    )

    print("result part one")
    print(calculate_path_cost(sorted_paths[-1], nodes_with_edges))

    print("done with part one")

    print()
    print("part2")

    me_node = "me"
    nodes_with_edges[me_node] = {node: 0 for node in nodes}

    nodes.append(me_node)
    first = nodes[0]
    rest = nodes[1:]

    all_paths = [[first] + list(path) for path in itertools.permutations(rest)]

    path_lengths = [calculate_path_cost(path, nodes_with_edges) for path in all_paths]
    sorted_paths = sorted(
        all_paths, key=(lambda path: calculate_path_cost(path, nodes_with_edges))
    )

    print("result part two")
    print(calculate_path_cost(sorted_paths[-1], nodes_with_edges))


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("done with test")

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
