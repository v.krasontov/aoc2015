import copy
import sys
import math
from dataclasses import dataclass
from itertools import groupby
from typing import Optional
from functools import reduce, cmp_to_key


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def main():
    print("part1")
    lines = readFile(sys.argv[1])

    floor = 0
    index = 0
    searching_basement = True
    for char in lines[0]:
        if char == "(":
            floor += 1
        if char == ")":
            floor -= 1
        index += 1
        if floor == -1 and searching_basement:
            print(index)
            searching_basement = False
    print(floor)


if __name__ == "__main__":
    main()
