import copy
import doctest
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum
from typing import Callable, Optional, overload

import numpy as np


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


@dataclass
class Ingredient:
    cap: int
    dur: int
    flav: int
    texture: int
    calories: int


def parse_line(line: str) -> Ingredient:
    words = line.split(" ")
    props = [words[2], words[4], words[6], words[8], words[10]]
    return Ingredient(*[int(prop.removesuffix(",")) for prop in props])


# capacity (how well it helps the cookie absorb milk)
# durability (how well it keeps the cookie intact when full of milk)
# flavor (how tasty it makes the cookie)
# texture (how it improves the feel of the cookie)
# calories (how many calories it adds to the cookie)

# Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
# Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3


def total_score(amounts: list[int], ingredients: list[Ingredient]) -> int:
    """
    >>> total_score([44, 56], [Ingredient(-1, -2, 6, 3, 8), Ingredient(2,3,-2,-1,3)])
    62842880
    """
    # assert sum(amounts) == 100
    assert len(amounts) == len(ingredients)
    total_cap = max(
        0,
        sum(
            [
                amount * ingredient.cap
                for amount, ingredient in zip(amounts, ingredients)
            ]
        ),
    )
    total_dur = max(
        0,
        sum(
            [
                amount * ingredient.dur
                for amount, ingredient in zip(amounts, ingredients)
            ]
        ),
    )
    total_flav = max(
        0,
        sum(
            [
                amount * ingredient.flav
                for amount, ingredient in zip(amounts, ingredients)
            ]
        ),
    )
    total_texture = max(
        0,
        sum(
            [
                amount * ingredient.texture
                for amount, ingredient in zip(amounts, ingredients)
            ]
        ),
    )
    #  total score of a cookie can be found by adding up each of the properties
    #  (negative totals become 0) and then multiplying together everything
    #  except calories.
    return total_cap * total_dur * total_flav * total_texture


def calory_count(amounts: list[int], ingredients: list[Ingredient]) -> int:
    assert len(amounts) == len(ingredients)
    return sum(
        [
            amount * ingredient.calories
            for amount, ingredient in zip(amounts, ingredients)
        ]
    )


def try_combinations(
    ingredients: list[Ingredient], number: int, mind_calories: bool = False
) -> int:
    numbers = range(1, number + 1)
    all_combis = itertools.product(numbers, numbers, numbers, numbers)
    valid_combis = [
        [a, b, c, d] for a, b, c, d in all_combis if a + b + c + d == number
    ]
    print(f"{len(valid_combis)=}")
    score = 0
    for combi in valid_combis:
        new_score = total_score(combi, ingredients)
        if new_score > score:
            if mind_calories:
                if calory_count(combi, ingredients) != 500:
                    continue
            score = new_score

    return score


def do_part_one(lines: list[str]) -> int:
    number_ingredients = 100

    ingredients = [parse_line(line) for line in lines]

    result = try_combinations(ingredients, number_ingredients)
    # Given the ingredients in your kitchen and their properties, what is the
    # total score of the highest-scoring cookie you can make?

    return result


def do_part_two(lines: list[str]) -> int:
    number_ingredients = 100

    ingredients = [parse_line(line) for line in lines]

    result = try_combinations(
        ingredients, number_ingredients, mind_calories=True)
    # Given the ingredients in your kitchen and their properties, what is the
    # total score of the highest-scoring cookie you can make?

    return result


def main(input_file: str) -> None:
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    result_part_one = do_part_one(lines)
    print(result_part_one)
    print("done with part one")

    print()
    print("part2")
    result_part_two = do_part_two(lines)
    print(result_part_two)
    print("done with part two")


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("done with test")

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
