import copy
import doctest
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum
from typing import Callable, Optional, overload

import numpy as np


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def parseAnalysis(lines: list[str]) -> dict[str, int]:
    """ cars: 2 """
    results = {}
    for line in lines:
        key, value = line.split(" ")
        results[key.removesuffix(":")] = int(value.strip())
    return results


def parseAunt(line: str) -> dict[str, int]:
    """
    >>> parseAunt('Sue 1: goldfish: 6, trees: 9, akitas: 0')
    {'goldfish': 6, 'trees': 9, 'akitas': 0}
    """
    aunt = {}
    props = [prop.strip() for prop in ":".join(line.split(":")[1:]).split(",")]
    for prop in props:
        key_value = prop.split(":")
        assert len(key_value) == 2
        key, value = key_value[0], key_value[1]
        aunt[key] = int(value.strip())
    return aunt


# children, by human DNA age analysis.
# cats. It doesn't differentiate individual breeds.
# Several seemingly random breeds of dog: samoyeds, pomeranians, akitas, and vizslas.
# goldfish. No other kinds of fish.
# trees, all in one group.
# cars, presumably by exhaust or gasoline or something.
# perfumes, which is handy, since many of your Aunts Sue wear a few kinds.

def keep_aunt(aunt: dict[str, int], analysis: dict[str, int]) -> bool:
    for key, value in analysis.items():
        if key not in aunt:
            continue
        if aunt[key] != value:
            return False
    return True


def keep_aunt_two(aunt: dict[str, int], analysis: dict[str, int]) -> bool:
    for key, value in analysis.items():
        if key not in aunt:
            continue
        # the cats and trees readings indicates that there are greater than that
        # many
        if key in ["cats", "trees"]:
            if aunt[key] <= value:
                return False
        # the pomeranians and goldfish readings indicate that there are
        # fewer than that many
        elif key in ["pomeranians", "goldfish"]:
            if aunt[key] >= value:
                return False
        elif aunt[key] != value:
            return False
    return True


def do_part_one(lines: list[str]) -> int:
    analysis_lines = []
    aunt_lines = []
    parsing_analysis = True
    for line in lines:
        if line == "":
            parsing_analysis = False
            continue
        if parsing_analysis:
            analysis_lines.append(line)
        else:
            aunt_lines.append(line)

    analysis_results = parseAnalysis(analysis_lines)
    aunts = [parseAunt(line) for line in aunt_lines]

    print(analysis_results)

    the_chosen_one = -1
    for aunt_index, aunt in enumerate(aunts):
        print(aunt)
        possible_aunt = keep_aunt(aunt, analysis_results)
        if possible_aunt:
            the_chosen_one = aunt_index
            print("we found a good one!")

    assert the_chosen_one > -1

    return the_chosen_one + 1


def do_part_two(lines: list[str]) -> int:
    analysis_lines = []
    aunt_lines = []
    parsing_analysis = True
    for line in lines:
        if line == "":
            parsing_analysis = False
            continue
        if parsing_analysis:
            analysis_lines.append(line)
        else:
            aunt_lines.append(line)

    analysis_results = parseAnalysis(analysis_lines)
    aunts = [parseAunt(line) for line in aunt_lines]

    print(analysis_results)

    the_chosen_one = -1
    for aunt_index, aunt in enumerate(aunts):
        print(aunt)
        possible_aunt = keep_aunt_two(aunt, analysis_results)
        if possible_aunt:
            the_chosen_one = aunt_index
            print("we found a good one!")

    assert the_chosen_one > -1

    return the_chosen_one + 1


def main(input_file: str) -> None:
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    result_part_one = do_part_one(lines)
    print(result_part_one)
    print("done with part one")

    print()
    print("part2")
    result_part_two = do_part_two(lines)
    print(result_part_two)
    print("done with part two")


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("done with test")

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
