import copy
import functools
import itertools
import json
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass
from typing import Any, Optional


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def debug(something):
    print(f"{something=}")


def do_something(x: int) -> bool:
    """do_something is an example for a function
    >>> do_something(4)
    False
    >>> do_something(5)
    True
    """
    return bool(x % 2)


def scan_for_red(some_json: Any) -> Any:
    if isinstance(some_json, dict):
        if "red" in some_json.values():
            return {}
        for key, value in some_json.items():
            some_json[key] = scan_for_red(value)
        return some_json
    if isinstance(some_json, list):
        for index, el in enumerate(some_json):
            some_json[index] = scan_for_red(el)
        return some_json
    return some_json


def main(input_file: str):
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    json_input = lines[0]
    parsed_json = json.loads(json_input)

    filtered_json = scan_for_red(parsed_json)
    with open("filtered-exam-input-p2", mode="w", encoding="utf-8") as fp:
        json.dump(filtered_json, fp)


if __name__ == "__main__":
    import doctest

    doctest.testmod()

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
