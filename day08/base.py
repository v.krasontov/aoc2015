import copy
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict
from dataclasses import dataclass
from typing import Optional


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


# "" is 2 characters of code (the two double quotes), but the string contains zero characters.
# "abc" is 5 characters of code, but 3 characters in the string data.
# "aaa\"aaa" is 10 characters of code, but the string itself contains six "a" characters and a single, escaped quote character, for a total of 7 characters in the string data.
# "\x27" is 6 characters of code, but the string itself contains just one - an
# apostrophe ('), escaped using hex


def code_length(x: str) -> int:
    """
    # >>> code_length('""')
    # 2
    # >>> code_length('"abc"')
    # 5
    # >>> code_length("aaa\"aaa")
    # 10
    # >>> code_length('"\x27"')
    # 6
    """
    return len(x)


def string_length(word: str) -> int:
    if word == "":
        return 0
    char = word[0]
    # print(f"looking at `{word}`, first char is `{char}`")
    if char != "\\":
        return 1 + string_length(word[1:])
    else:
        next_char = word[1]
        if next_char in ["\\", '"']:
            return 1 + string_length(word[2:])
        if next_char == "x":
            assert len(word) >= 4
            return 1 + string_length(word[4:])
    raise RuntimeError("unexpected case!")


# contains many double-quoted string literals, one on each line. The only escape
# sequences used are \\ (which represents a single backslash), \" (which
# represents a lone double-quote character), and \x plus two hexadecimal
# characters (which represents a single character with that ASCII code).


def encoded_length(word: str) -> int:
    if word == "":
        return 0
    char = word[:1]
    # print(f"looking at `{word}`, first char is `{char}`")
    if char in ['"', "\\"]:
        # print("need to encode!")
        return 2 + encoded_length(word[1:])
    return 1 + encoded_length(word[1:])


def main(input_file: str):
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    """
    for line in lines:
        print()
        print(line)
        print(f"{code_length(line)=}")
        actual_string_length = string_length(line.strip('"'))
        print(f"{actual_string_length=}")
    """

    """  what is the number of characters of code for string literals minus the
         number of characters in memory for the values of the strings       """

    print(sum(code_length(line) - string_length(line[1:-1]) for line in lines))

    print("part2")

    for line in lines:
        print(f"{line} - {2 + encoded_length(line)=}")

    print(sum(2 + encoded_length(line) - code_length(line) for line in lines))


if __name__ == "__main__":
    import doctest

    doctest.testmod()

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
