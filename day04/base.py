import copy
import sys
import math
from dataclasses import dataclass
from itertools import groupby
from typing import Optional
from functools import reduce, cmp_to_key


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


import hashlib


def main():
    print("part1")
    lines = readFile(sys.argv[1])
    key = lines[0]
    print(key)

    test = b"abcdef609043"
    print(test)
    print(hashlib.md5(test).hexdigest())

    digest = ""
    number = 0
    while not digest.startswith("000000"):
        number += 1
        input_string = bytes(key + str(number), "utf-8")
        # print(input_string)
        digest = hashlib.md5(input_string).hexdigest()
        # print(digest)
    print(number)


if __name__ == "__main__":
    main()
