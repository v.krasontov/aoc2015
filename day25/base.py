import copy
import doctest
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum
from typing import Callable, Optional, overload


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def parseLine(line: str) -> None:
    ...


def get_next_code(previous: int, multi: int, mod: int) -> int:
    return (previous * multi) % mod


def get_code_at_index(index: int, first: int, multi: int, mod: int) -> int:
    """
    >>> get_code_at_index(1, 20151125, 252533, 33554393)
    20151125
    >>> get_code_at_index(2, 20151125, 252533, 33554393)
    31916031
    >>> get_code_at_index(3, 20151125, 252533, 33554393)
    18749137
    >>> get_code_at_index(find_index(5,1), 20151125, 252533, 33554393)
    77061
    """
    code = first
    counter = 1
    while counter != index:
        code = get_next_code(code, multi, mod)
        counter += 1
    return code


def get_period(multi: int, mod: int) -> int:
    period = 1
    powers = 1
    while True:
        powers *= multi
        powers %= mod
        if powers == 1:
            return period
        period += 1


def find_index(row: int, column: int) -> int:
    """
    >>> find_index(1,1)
    1
    >>> find_index(1,5)
    15
    >>> find_index(1,10)
    55
    >>> find_index(3,4)
    19
    >>> find_index(6,1)
    16
    """
    if row == 1:
        return column * (column + 1) // 2
    return find_index(1, column) + sum(range(column, column + row - 1))


def find_index_naive(row: int, column: int) -> int:
    running_row = 1
    running_column = 1
    index = 1
    while True:
        # breakpoint()
        # print(running_row, running_column, index)
        if running_row == row and running_column == column:
            return index
        if running_row == 1:  # reached upper right corner, going down left
            running_row = running_column + 1
            running_column = 1
        else:
            running_row -= 1
            running_column += 1
        index += 1
    return index


def do_part_one(lines: list[str]) -> int:
    row, column = int(lines[0]), int(lines[1])
    print(row, column)
    first = 20151125
    multi = 252533
    mod = 33554393
    period = get_period(multi, mod)
    print(f"period is {period}")
    index = find_index(row, column)
    print(f"{index=}")
    # modded_index = index % period
    modded_index = index
    print(f"{modded_index=}")
    return get_code_at_index(modded_index, first, multi, mod)


# too high: 14172823, first attempt


def do_part_two(lines: list[str]) -> int:
    return 0


def main(input_file: str) -> None:
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    result_part_one = do_part_one(lines)
    print(result_part_one)
    print("done with part one")

    print()
    print("part2")
    print("sds")
    result_part_two = do_part_two(lines)
    print(result_part_two)
    print("done with part two")


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("done with test")

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
