import copy
import sys
import math
from dataclasses import dataclass
from itertools import groupby
from typing import Optional
from functools import reduce, cmp_to_key


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def visit(dic, x, y):
    dic[(x, y)] = dic.get((x, y), 0) + 1


def update(step, x, y):
    if step == "^":
        y += 1
    elif step == "v":
        y -= 1
    elif step == ">":
        x += 1
    elif step == "<":
        x -= 1
    return x, y


def main():
    print("part1")
    steps = readFile(sys.argv[1])[0]
    visited = {}
    x = 0  # east-west
    y = 0  # north-south
    visited[(x, y)] = visited.get((x, y), 0) + 1
    for direction in steps:
        # north (^), south (v), east (>), or west (<)
        x, y = update(direction, x, y)
        visit(visited, x, y)
    print(len(visited))

    print("part2")
    visited = {}
    real_x = 0  # east-west
    real_y = 0  # north-south
    robo_x = 0
    robo_y = 0
    visit(visited, real_x, real_y)
    visit(visited, robo_x, robo_y)
    for index in range(len(steps)):
        step = steps[index]
        if index % 2:
            real_x, real_y = update(step, real_x, real_y)
            visit(visited, real_x, real_y)
        else:
            robo_x, robo_y = update(step, robo_x, robo_y)
            visit(visited, robo_x, robo_y)
    print(len(visited))


if __name__ == "__main__":
    main()
