import copy
import doctest
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum, StrEnum
from typing import Any, Callable, Optional, overload

try:
    from util import dijkstra
except ModuleNotFoundError:
    sys.path.append("..")
    from util import dijkstra


DEBUG = False
PART_TWO = False


def cursive(some: str) -> str:
    return f"\x1B[3m{some}\x1B[0m"


class SpellName(StrEnum):
    MAGIC_MISSILE = "magic_missile"
    DRAIN = "drain"
    SHIELD = "shield"
    POISON = "poison"
    RECHARGE = "recharge"

    def __str__(self: "SpellName") -> str:
        return self.value

    def __repr__(self: "SpellName") -> str:
        return self.value


@dataclass
class ActiveEffect:
    name: SpellName
    duration_left: int
    action: Optional[Callable[["GameState"], "GameState"]]

    def __str__(self: "ActiveEffect") -> str:
        return f"{self.name} - {self.duration_left}"

    def __repr__(self: "ActiveEffect") -> str:
        return self.__str__()


@dataclass
class PC:
    hitpoints: int = 50
    mana: int = 500


@dataclass
class Boss:
    hitpoints: int = 55
    damage: int = 8


@dataclass
class GameState:
    pc: PC
    boss: Boss
    active_effects: list[ActiveEffect]
    used_spells: list[SpellName]

    def __hash__(self: "GameState") -> int:
        return hash(tuple(self.used_spells))

    def __eq__(self: "GameState", other: object) -> bool:
        if not isinstance(other, GameState):
            return NotImplemented
        return self.used_spells == other.used_spells

    def __str__(self: "GameState") -> str:
        names_active_effects = [effect.name for effect in self.active_effects]
        return (
            f"-- {self.pc}\n"
            f"-- {self.boss}\n"
            f"-- active effects: {self.active_effects}\n"
            f"-- used spells: {self.used_spells}\n"
            ""
        )


@dataclass
class Spell:
    name: SpellName
    mana_cost: int
    duration: Optional[int]
    is_effect: bool
    action: Optional[Callable[["GameState"], "GameState"]]


def magic_miss_func(state: GameState) -> GameState:
    state.boss.hitpoints -= 4
    return state


def drain_func(state: GameState) -> GameState:
    state.boss.hitpoints -= 2
    state.pc.hitpoints += 2
    return state


def poison_func(state: GameState) -> GameState:
    state.boss.hitpoints -= 3
    return state


def recharge_func(state: GameState) -> GameState:
    state.pc.mana += 101
    return state


magic_missile: Spell = Spell(
    SpellName.MAGIC_MISSILE,
    53,
    None,
    False,
    magic_miss_func)
drain: Spell = Spell(SpellName.DRAIN, 73, None, False, drain_func)
shield: Spell = Spell(SpellName.SHIELD, 113, 6, True, None)
poison: Spell = Spell(SpellName.POISON, 173, 6, True, poison_func)
recharge: Spell = Spell(SpellName.RECHARGE, 229, 5, True, recharge_func)

SPELLS: list[Spell] = [magic_missile, drain, shield, poison, recharge]

SPELLS_BY_NAME: dict[SpellName, Spell] = {
    spell.name: spell for spell in SPELLS}


def start_state() -> GameState:
    start = GameState(pc=PC(), boss=Boss(), active_effects=[], used_spells=[])
    return start


def do_two_turns(state: GameState, spell: Spell, hard_mode: bool) -> GameState:
    # do_two_turns should not modify original state!
    new_state = copy.deepcopy(state)
    return boss_turn(player_turn(new_state, spell, hard_mode))


def do_effects(state: GameState) -> GameState:
    for active_effect in state.active_effects:
        if active_effect.action is not None:
            state = active_effect.action(state)
        active_effect.duration_left -= 1
    state.active_effects = [
        active_effect
        for active_effect in state.active_effects
        if active_effect.duration_left > 0
    ]
    return state


def boss_dead(state: GameState) -> bool:
    return state.boss.hitpoints <= 0


def player_dead(state: GameState) -> bool:
    return state.pc.hitpoints <= 0


def player_turn(state: GameState, spell: Spell, hard_mode: bool) -> GameState:
    if DEBUG:
        print(f"player's turn - gonna cast {cursive(spell.name)}")
        print(state)
    if hard_mode:
        state.pc.hitpoints -= 1
        if player_dead(state):
            if DEBUG:
                print("player is dead 😿")
            return state
    state = do_effects(state)

    if boss_dead(state):
        if DEBUG:
            print("boss is dead!")
        return state

    state.pc.mana -= spell.mana_cost

    if spell.is_effect:
        assert isinstance(spell.duration, int)
        state.active_effects.append(
            ActiveEffect(spell.name, spell.duration, spell.action)
        )
    else:
        assert spell.action is not None
        state = spell.action(state)
        if DEBUG and boss_dead(state):
            print("boss is dead 🎉")
    state.used_spells.append(spell.name)
    return state


def boss_turn(state: GameState) -> GameState:
    if DEBUG:
        print("boss's turn")
        print(state)
    state = do_effects(state)

    if boss_dead(state):
        if DEBUG:
            print("boss is dead 🎉")
        return state

    names_active_effects = [effect.name for effect in state.active_effects]
    armor = 7 if "shield" in names_active_effects else 0
    state.pc.hitpoints -= max(1, state.boss.damage - armor)
    if DEBUG and player_dead(state):
        print("player is dead 😿")
    return state


def effect_can_be_cast(state: GameState, spell_name: str) -> bool:
    # if spell is effect and effect is in place and
    #   has duration left > 1, skip
    active_effects_with_spell_name = [
        effect for effect in state.active_effects if effect.name == spell_name
    ]
    if len(active_effects_with_spell_name) == 0:
        return True
    active_effect = active_effects_with_spell_name[0]
    if active_effect.duration_left > 1:
        return False
    return True


def get_next_state_func(
        hard_mode: bool) -> Callable[[GameState], list[GameState]]:
    def next_possible_states(state: GameState) -> list[GameState]:
        next_states = []
        # for all spells
        for spell in SPELLS:
            # if spell is too expensive, skip
            if spell.mana_cost > state.pc.mana:
                continue
            # if spell is effect and effect is in place and
            #   has duration left > 1, skip
            if spell.is_effect and not effect_can_be_cast(state, spell.name):
                continue
            # compute new state for spell
            new_state = do_two_turns(state, spell, hard_mode)
            # if PC is dead, don't include state
            if player_dead(new_state):
                continue
            # otherwise include state in result
            next_states.append(new_state)
        return next_states

    return next_possible_states


def done(state: GameState) -> bool:
    return boss_dead(state)


def get_distance(_: GameState, state: GameState) -> int:
    return SPELLS_BY_NAME[state.used_spells[-1]].mana_cost


def mana_spent(state: GameState) -> int:
    return sum(
        SPELLS_BY_NAME[spell_name].mana_cost for spell_name in state.used_spells)


def demo_one() -> None:
    print("demo one")
    state = GameState(PC(10, 250), Boss(13, 8), [], [])
    spells = [SpellName.POISON, SpellName.MAGIC_MISSILE]
    print()
    for spell_name in spells:
        state = do_two_turns(state, SPELLS_BY_NAME[spell_name])
    print("demo one finished")


def demo_two() -> None:
    print("demo two")
    state = GameState(PC(10, 250), Boss(14, 8), [], [])
    spells = [
        SpellName.RECHARGE,
        SpellName.SHIELD,
        SpellName.DRAIN,
        SpellName.POISON,
        SpellName.MAGIC_MISSILE,
    ]
    print()
    for spell_name in spells:
        state = do_two_turns(state, SPELLS_BY_NAME[spell_name])
    print("demo two finished")


def do_part_one() -> int:
    # demo_one()
    # demo_two()
    initial_state = GameState(PC(50, 500), Boss(55, 8), [], [])
    next_possible_states = get_next_state_func(hard_mode=False)
    distances = dijkstra(
        initial_state,
        next_possible_states,
        get_distance,
        done)
    won_states = [state for state in distances.keys() if boss_dead(state)]
    return int(min(distances[won_state] for won_state in won_states))


def do_part_two() -> int:
    initial_state = GameState(PC(50, 500), Boss(55, 8), [], [])
    next_possible_states = get_next_state_func(hard_mode=True)
    distances = dijkstra(
        initial_state,
        next_possible_states,
        get_distance,
        done)
    won_states = [state for state in distances.keys() if boss_dead(state)]
    return int(min(distances[won_state] for won_state in won_states))


def main() -> None:
    print("part1")
    result_part_one = do_part_one()
    print(result_part_one)
    print("done with part one")
    print()

    print("part2")
    PART_TWO = True
    result_part_two = do_part_two()
    print(result_part_two)
    print("done with part two")


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("doctests were successful!")

    main()
