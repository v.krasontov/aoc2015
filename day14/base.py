import copy
import doctest
import functools
import itertools
import math
import numpy as np
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum
from typing import Callable, Optional, overload


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


@dataclass
class Reindeer:
    speed_per_second: int
    travel_before_rest: int
    rest_duration: int


def parse_line(line: str) -> Reindeer:
    words = line.split(" ")
    speed, travel, rest = int(words[3]), int(words[6]), int(words[-2])
    return Reindeer(speed, travel, rest)


def travelled_distance(rein: Reindeer, duration: int) -> int:
    """
    >>> travelled_distance(Reindeer(2, 2, 2), 4)
    4
    >>> travelled_distance(Reindeer(2, 2, 2), 3)
    4
    >>> travelled_distance(Reindeer(2, 2, 2), 5)
    6
    >>> travelled_distance(Reindeer(2, 2, 2), 6)
    8
    >>> travelled_distance(Reindeer(2, 2, 2), 7)
    8
    """
    travel_rest_duration = rein.travel_before_rest + rein.rest_duration
    travel_rest_periods = duration // travel_rest_duration
    assert travel_rest_periods * rein.travel_before_rest <= duration
    assert travel_rest_periods * rein.rest_duration <= duration
    easy_part = rein.speed_per_second * rein.travel_before_rest * travel_rest_periods
    rest_period = duration % travel_rest_duration
    assert rest_period < rein.travel_before_rest + rein.rest_duration
    complicated_part = rein.speed_per_second * min(rest_period, rein.travel_before_rest)

    return easy_part + complicated_part


def do_part_one(lines: list[str]) -> int:
    seconds = 2503
    reindeers = [parse_line(line) for line in lines]

    travelled_distances = [travelled_distance(rein, seconds) for rein in reindeers]

    # what distance has the winning reindeer traveled?
    return max(travelled_distances)


def do_part_two(lines: list[str]) -> int:
    seconds = 2503
    reindeers = [parse_line(line) for line in lines]

    points = [0 for rein in reindeers]
    for seconds_travelled in range(1, seconds + 1):
        travelled_distances = [
            travelled_distance(rein, seconds_travelled) for rein in reindeers
        ]
        # print("done calculating distances")
        max_travelled = max(travelled_distances)
        winning_deer = -1
        print(travelled_distances)
        while True:
            # print("in while loop")
            try:
                winning_deer = travelled_distances.index(
                    max_travelled, winning_deer + 1
                )
                # print(f"found winning deer at {winning_deer}")
                points[winning_deer] += 1
            except ValueError as e:
                # print(f"got error {e}")
                break
        # print("done with while loop")
        # print(f"travelled {seconds_travelled} seconds")

    # what distance has the winning reindeer traveled?
    print("done with part two")
    return max(points)


# Reindeer can only either be flying (always at their top speed) or resting
# (not moving at all), and always spend whole seconds in either state.


def main(input_file: str) -> None:
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    result_part_one = do_part_one(lines)
    print(result_part_one)
    print("done with part one")

    print()
    print("part2")
    print(do_part_two(lines))


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("done with test")

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
