import copy
import doctest
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum
from typing import Callable, Optional, overload


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def count_the_ways(to_fill: int, containers: list[int]) -> int:
    print(f"{to_fill=}, {containers=}")
    if to_fill == 0:
        return 1
    if to_fill > sum(containers):
        return 0
    copy_containers = copy.copy(containers)
    first = copy_containers.pop(0)
    if first > to_fill:
        using = 0
    else:
        using = count_the_ways(to_fill - first, copy_containers)
    return count_the_ways(to_fill, copy_containers) + using


def find_the_ways(
        to_fill: int,
        containers_used: list[int],
        containers_left: list[int]) -> list[list[int]]:
    print(f"{to_fill=}, {containers_left=}")
    if to_fill == 0:
        return [containers_used]
    if to_fill > sum(containers_left):
        return []
    first = containers_left.pop(0)
    if first > to_fill:
        using = []
    else:
        copy_containers_used = copy.copy(containers_used)
        copy_containers_used.append(first)
        using = find_the_ways(
            to_fill - first,
            copy_containers_used,
            copy.copy(containers_left))
    return find_the_ways(
        to_fill,
        containers_used,
        containers_left) + using


def do_part_one(lines: list[str], total: int = 150) -> int:
    containers = sorted([int(line) for line in lines], reverse=True)

    return count_the_ways(total, containers)


def do_part_two(lines: list[str], total: int = 150) -> int:
    containers = sorted([int(line) for line in lines], reverse=True)

    all_ways = find_the_ways(total, [], containers)
    for way in all_ways:
        print(way)
        assert sum(way) == total
    all_ways_sorted = list(
        sorted(
            all_ways,
            key=(
                lambda x: len(x)),
        ))

    shortest = len(all_ways_sorted[0])

    print(len(all_ways_sorted))

    shortest_ways = [way for way in all_ways_sorted if len(way) == shortest]
    for way in shortest_ways:
        print(way)
    return len(shortest_ways)


def main(input_file: str) -> None:
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
        total = 25 if sys.argv[1] == "test-input" else 150
    else:
        lines = []
        total = 150

    result_part_one = do_part_one(lines, total)
    print(result_part_one)
    print("done with part one")

    print()
    print("part2")
    result_part_two = do_part_two(lines, total)
    print(result_part_two)
    print("done with part two")


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("done with test")

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
