import copy
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass
from typing import Optional


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def debug(something):
    print(f"{something=}")


def do_something(x: int) -> bool:
    """do_something is an example for a function
    >>> do_something(4)
    False
    >>> do_something(5)
    True
    """
    return bool(x % 2)


def increment_letter(letter: str) -> str:
    return chr(ord(letter) + 1)


def increment(password: str) -> str:
    # Increase the rightmost letter one step; if it was z, it wraps around to
    # a, and repeat with the next letter to the left until one doesn't wrap
    # around.
    if password == len(password) * "z":
        raise RuntimeError("overflow of password length!")
    last_letter = password[-1]
    if last_letter == "z":
        return increment(password[:-1]) + "a"
    new_letter = increment_letter(last_letter)
    if new_letter in ["i", "o", "l"]:
        new_letter = increment_letter(new_letter)
    return password[:-1] + new_letter


def get_straights_regex() -> re.Pattern:
    straight_starts = string.ascii_lowercase[:-2]
    straights_ascii = [
        list(range(ord(start), ord(start) + 3)) for start in straight_starts
    ]
    straights = [
        "".join(map(chr, straight_ascii)) for straight_ascii in straights_ascii
    ]
    # print(straights)
    straights_regex = re.compile("|".join(straights))
    return straights_regex


# def fast_update


def valid(password: str) -> tuple[str, bool]:
    # exactly eight lowercase letters
    if len(password) != 8:
        return password, False
    # may not contain the letters i, o, or l
    if len(re.findall("i|o|l", password)) > 0:
        # TODO add fast update!
        return password, False
    # one increasing straight of at least three letters, like abc, bcd, cde,
    # and so on, up to xyz. They cannot skip letters; abd doesn't count.
    straights_regex = get_straights_regex()
    # print(straights_regex)
    assert len(straights_regex.findall("hahahahcdeadkmald")) == 1
    if len(straights_regex.findall(password)) == 0:
        return password, False
    # at least two different, non-overlapping pairs of letters, like aa, bb, or
    # zz
    pairs_regex_patterns = [2 * letter for letter in string.ascii_lowercase]
    pairs_numbers = [
        len(re.findall(pattern, password)) for pattern in pairs_regex_patterns
    ]
    if len([1 for pair_number in pairs_numbers if pair_number > 0]) < 2:
        return password, False

    return password, True


def find_next_password(password: str) -> str:
    while True:
        password = increment(password)
        password, is_valid = valid(password)
        if is_valid:
            return password


def main(input_file: str):
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    password = lines[0]
    print(password)
    next_password = find_next_password(password)
    print(next_password)

    print("part2")
    next_next_password = find_next_password(next_password)
    print(next_next_password)


if __name__ == "__main__":
    import doctest

    doctest.testmod()

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
