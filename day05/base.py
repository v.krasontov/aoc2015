import copy
import sys
import math
import re
import string
import itertools
import functools
from dataclasses import dataclass
from typing import Optional


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def number_vowels(s: str) -> int:
    reg = re.compile("a|e|i|o|u")
    return len(reg.findall(s))


def has_double(s: str) -> bool:
    reg = re.compile("|".join([letter + "{2}" for letter in string.ascii_lowercase]))
    return len(reg.findall(s)) > 0


def no_bad(s: str) -> bool:
    one = re.compile("ab|cd|pq|xy")
    return len(one.findall(s)) == 0


def nice(s: str) -> bool:
    return number_vowels(s) >= 3 and has_double(s) and no_bad(s)


def has_pair(s: str) -> bool:
    """It contains a pair of any two letters that appears at least twice in
    the string without overlapping, like xyxy (xy) or aabcdefgaa (aa), but not
    like aaa (aa, but it overlaps)."""
    pairs = [
        one + other
        for one, other in itertools.product(
            string.ascii_lowercase, string.ascii_lowercase
        )
    ]
    for pair in pairs:
        if len(re.findall(pair, s)) >= 2:
            return True
    return False


def around(s: str) -> bool:
    """It contains at least one letter which repeats with exactly one letter
    between them, like xyx, abcdefeghi (efe), or even aaa."""
    for letter in string.ascii_lowercase:
        if len(re.findall(f"{letter}.{letter}", s)) > 0:
            return True
    return False


def other_nice(s: str) -> bool:
    return has_pair(s) and around(s)


def main():
    print("part1")
    lines = readFile(sys.argv[1])
    assert nice("ugknbfddgicrmopn") == True
    assert nice("aaa") == True
    assert nice("jchzalrnumimnmhp") == False

    print(len([line for line in lines if nice(line)]))

    print("part2")
    assert has_pair("xyxy")
    assert has_pair("aafjdsnkgaa")
    assert not has_pair("xxx")
    assert not has_pair("xaxbxyxz")

    assert around("ada")
    assert around("abcdefeghi")
    assert around("aaa")
    assert not around("abc")

    print(len([line for line in lines if other_nice(line)]))
    assert other_nice("qjhvhtzxzqqjkmpb")
    assert other_nice("xxyxx")
    assert not other_nice("uurcxstgmygtbstg")
    assert not other_nice("ieodomkazucvgmuy")


# For example:
#
# qjhvhtzxzqqjkmpb is nice because is has a pair that appears twice (qj) and a letter that repeats with exactly one letter between them (zxz).
# xxyxx is nice because it has a pair that appears twice and a letter that repeats with one between, even though the letters used by each rule overlap.
# uurcxstgmygtbstg is naughty because it has a pair (tg) but no repeat with a single letter between them.
# ieodomkazucvgmuy is naughty because it has a repeating letter with one between (odo),
# but no pair that appears twice.

if __name__ == "__main__":
    main()
