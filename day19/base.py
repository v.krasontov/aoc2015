import copy
import doctest
import functools
import heapq
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum
from typing import Callable, Optional, overload

import numpy as np


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def parse_transform(line: str) -> tuple[str, str]:
    first, _, second = line.split(" ")
    return first, second


def replacements(first: str, second: str, orig: str) -> list[str]:
    """
    >>> replacements('H', 'BAR', 'HOHO')
    ['BAROHO', 'HOBARO']
    >>> replacements('H', 'HO', 'HOH')
    ['HOOH', 'HOHO']
    """
    start = -1
    results = []
    while True:
        start = orig.find(first, start + 1)
        if start == -1:
            break
        results.append(orig[:start] + orig[start:].replace(first, second, 1))
    return results


def get_all_transforms(
        orig: str, transforms: list[tuple[str, str]]) -> set[str]:
    possibilities = set()
    for first, second in transforms:
        possibilities.update(replacements(first, second, orig))
    return possibilities


def do_part_one(lines: list[str]) -> int:
    lines = copy.copy(lines)
    orig = lines.pop()
    lines.pop()
    transforms = [parse_transform(line) for line in lines]
    possibilities = get_all_transforms(orig, transforms)
    return len(possibilities)


def get_number_of_capitals(some: str) -> int:
    return sum(some.count(capital) for capital in string.ascii_uppercase)


def dijkstra(
    start: str,
    get_neighbours: Callable[[str], list[str]],
    get_point_distance: Callable[[str, str], int],
    get_priority: Callable[[str], int],
    done: Callable[[str], bool],
    goal: str,
) -> dict[str, int]:
    visited: dict[str, bool] = {}
    # distances: dict[str, int] = defaultdict(
    #     default_factory=(lambda: math.inf))
    # distances[start] = 0
    distances: dict[str, int] = {start: 0}
    queue: list[str] = []
    heapq.heappush(queue, (2, start))
    # breakpoint()
    while len(queue) > 0:
        _, current = heapq.heappop(queue)
        if current in visited:
            continue
        if done(current):
            break
        distance_to_current = distances[current]
        distance_to_goal = get_priority(current)
        prefix_match_length = length_matching_prefixes(current, goal)
        print(f"looking at {current}")
        print(
            f"visited: {len(visited):10}, "
            f"capitals in prefix: {get_number_of_capitals(current[:prefix_match_length])}, "
            f"capitals in current: {get_number_of_capitals(current)}, "
            f"dist: {distance_to_current:4}, "
            f"dist to goal: {distance_to_goal:2}, "
            f"prefix match: {prefix_match_length:4}, "
            f"length: {len(current):4}")
        # if prefix_match_length >= 384:
        #     break
        neighbours = [
            neighbour
            for neighbour in get_neighbours(current)
            if neighbour not in visited
        ]
        for neighbour in neighbours:
            distance_current_to_neighbour = get_point_distance(
                current, neighbour)
            new_distance_to_neighbour = (
                distance_to_current + distance_current_to_neighbour
            )
            heapq.heappush(
                queue,
                (new_distance_to_neighbour +
                 get_priority(neighbour),
                 neighbour))
            if (
                neighbour not in distances or
                new_distance_to_neighbour < distances[neighbour]
            ):
                distances[neighbour] = new_distance_to_neighbour
        visited[current] = True
    return distances


def length_matching_prefixes(one: str, other: str) -> int:
    """
    >>> length_matching_prefixes("hallo", "hamburg")
    2
    >>> length_matching_prefixes("hey", "you")
    0
    >>> length_matching_prefixes("hallo", "hallodu")
    5
    """
    for i in range(min(len(one), len(other))):
        if one[i] != other[i]:
            return i
    return i + 1


prefix_regex = re.compile("^[A-Z][a-z]?")


def find_reachable_prefixes_for_one_source(
    transforms: dict[str, list[str]], source: str
) -> list[str]:
    possible_symbols = set()
    seen: dict[str, bool] = {}
    queue: deque[str] = deque([source])
    while len(queue) > 0:
        current = queue.pop()
        if current in seen:
            continue
        if current not in transforms:
            seen[current] = True
            continue
        targets = transforms[current]
        prefixes = [prefix_regex.match(target).group() for target in targets]
        for prefix in prefixes:
            if prefix not in possible_symbols:
                queue.append(prefix)
        possible_symbols.update(prefixes)
        seen[current] = True
    return possible_symbols


def find_reachable_prefixes(transforms: list[str, str]) -> dict[str, list[str]]:
    transforms_dict: dict[str, list[str]] = {}
    for source, target in transforms:
        if source not in transforms_dict:
            transforms_dict[source] = [target]
        else:
            transforms_dict[source].append(target)

    reachable: dict[str, list[str]] = {}
    for source, _ in transforms:
        reachable[source] = find_reachable_prefixes_for_one_source(
            transforms_dict, source
        )
    return reachable


def do_part_two(lines: list[str]) -> int:
    orig = lines.pop()
    lines.pop()
    print(f"orig in part two: `{orig}`")
    transforms = [parse_transform(line) for line in lines]

    reachable_symbols = find_reachable_prefixes(transforms)
    for symbol, reachable in reachable_symbols.items():
        print(f"{symbol} can reach {reachable}")

    def count_left_rns(neighbour: str) -> int:
        rns_in_orig = orig.count("Rn")
        matching_prefix_length = length_matching_prefixes(neighbour, orig)
        rns_in_neighbours_matching_part = neighbour[:matching_prefix_length].count(
            "Rn")
        return rns_in_orig - rns_in_neighbours_matching_part

    def count_existing_non_matching_symbols(neighbour: str) -> int:
        prefix_length = length_matching_prefixes(neighbour, orig)
        capitals_in_unmatched_orig = get_number_of_capitals(
            orig[prefix_length:])
        # if summed < 100:
        #     return summed // max(len(target) for source, target in transforms)
        return capitals_in_unmatched_orig * 0.9

    def count_unmatched_letters(neighbour: str) -> int:
        prefix_length = length_matching_prefixes(neighbour, orig)
        return len(orig) - prefix_length

    def done_func(current: str) -> bool:
        return current.startswith(orig)
        # return current == orig

    def better_neighbours_func(current: str) -> str:
        prefix_length = length_matching_prefixes(current, orig)
        last_rn_in_matching_part = re.search(
            "Rn[^R]*$", current[:prefix_length])
        if last_rn_in_matching_part is None:
            transform_from = 0
        else:
            transform_from = last_rn_in_matching_part.start() + 2
        rest_transformations = get_all_transforms(
            current[transform_from:], transforms)
        return [current[:transform_from] + rest
                for rest in rest_transformations]

        # rest = current[prefix_length:]
        # possible_transform_sources = set([
        #     key for key, _ in transforms if rest.startswith(key)])
        # if len(possible_transform_sources) == 0:
        #     return []
        # assert len(possible_transform_sources) == 1
        # chosen_transform_source = possible_transform_sources.pop()
        # target_symbol = prefix_regex.match(orig[prefix_length:]).group()
        # transformed_bits = [
        #     result
        #     for source, result in transforms
        #     if (source == chosen_transform_source and
        #         target_symbol in reachable_symbols[source])]
        # unchanged_rest = rest[len(chosen_transform_source):]
        # neighbours = [current[:prefix_length] + transformed_bit + unchanged_rest
        #               for transformed_bit in transformed_bits]
        # for neighbour in neighbours:
        #     assert prefix_length <= length_matching_prefixes(neighbour, orig)
        #     print(neighbour)
        # print()
        # return neighbours

    def neighbours_func(current: str) -> str:
        return list(get_all_transforms(current, transforms))

    def distance_func(_: str, __: str) -> int:
        return 1

    # print("transform results contained in recipe:")
    # for _, result in transforms:
    #     if result in orig:
    #         print(result)
    # print()
    # return 0

    start = "e"
    distances_from_e = dijkstra(
        start,
        better_neighbours_func,
        distance_func,
        count_existing_non_matching_symbols,
        done_func,
        orig,
    )

    first_length = None
    for key, value in distances_from_e.items():
        if key.startswith(orig):
            if first_length is None:
                first_length = value
                print(f"smallest distance: {first_length}")
            if value == first_length:
                print(
                    f"{key} left rns: {count_left_rns(key)}, "
                    f"non-matching: {count_existing_non_matching_symbols(key)}"
                )
    # print(distances_from_e)

    return 0


def main(input_file: str) -> None:
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    result_part_one = do_part_one(lines)
    print(result_part_one)
    print("done with part one")

    print()
    print("part2")
    if not input_file == "test-input":
        result_part_two = do_part_two(lines)
        print(result_part_two)
        print("done with part two")


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("done with doctests!")

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
