import copy
import doctest
import functools
import heapq
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum
from typing import Callable, Optional, TypeVar, overload


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def parse_transform(line: str) -> tuple[str, str]:
    first, _, second = line.split(" ")
    return first, second


def replacements(first: str, second: str, orig: str) -> list[str]:
    """
    >>> replacements('H', 'BAR', 'HOHO')
    ['BAROHO', 'HOBARO']
    >>> replacements('H', 'HO', 'HOH')
    ['HOOH', 'HOHO']
    """
    start = -1
    results = []
    while True:
        start = orig.find(first, start + 1)
        if start == -1:
            break
        results.append(orig[:start] + orig[start:].replace(first, second, 1))
    return results


def get_all_transforms(
        orig: str, transforms: list[tuple[str, str]]) -> set[str]:
    possibilities = set()
    for first, second in transforms:
        possibilities.update(replacements(first, second, orig))
    return possibilities


def get_number_of_capitals(some: str) -> int:
    return sum(some.count(capital) for capital in string.ascii_uppercase)


NodeType = TypeVar("NodeType")


def astar(
    start: NodeType,
    get_neighbours: Callable[[NodeType], list[NodeType]],
    get_point_distance: Callable[[NodeType, NodeType], int | float],
    heuristic: Callable[[NodeType], int | float],
    done: Callable[[NodeType], bool],
    goal: NodeType,
) -> dict[NodeType, int | float]:
    visited: dict[NodeType, bool] = {}
    distances: dict[NodeType, int | float] = defaultdict(lambda: math.inf)
    distances[start] = 0
    queue: list[tuple[int | float, NodeType]] = []
    heapq.heappush(queue, (0 + heuristic(start), start))
    while len(queue) > 0:
        _, current = heapq.heappop(queue)
        if current in visited:
            continue
        if done(current):
            break
        distance_to_current = distances[current]
        distance_to_goal = heuristic(current)
        print(f"looking at {current}")
        print(
            f"visited: {len(visited):10}, "
            f"dist: {distance_to_current:4}, "
            f"heuristic dist to goal: {distance_to_goal:2}, "
        )
        neighbours = [
            neighbour
            for neighbour in get_neighbours(current)
            if neighbour not in visited
        ]
        for neighbour in neighbours:
            distance_current_to_neighbour = get_point_distance(
                current, neighbour)
            new_distance_to_neighbour = (
                distance_to_current + distance_current_to_neighbour
            )
            heapq.heappush(
                queue,
                (new_distance_to_neighbour +
                 heuristic(neighbour),
                    neighbour))
            if new_distance_to_neighbour < distances[neighbour]:
                distances[neighbour] = new_distance_to_neighbour
        visited[current] = True
    return distances


def dijkstra(
    start: NodeType,
    get_neighbours: Callable[[NodeType], list[NodeType]],
    get_point_distance: Callable[[NodeType, NodeType], int | float],
    done: Optional[Callable[[NodeType], bool]] = None,
) -> dict[NodeType, int | float]:
    """dijkstra performs the same algo on the given input, returning the
    dictionary of nodes and distances"""
    visited: dict[NodeType, bool] = {}
    distances: dict[NodeType, int | float] = defaultdict(lambda: math.inf)
    distances[start] = 0
    # entry counter is used to break ties for incomparable nodes - earlier
    # encountered nodes will be preferred
    queue_entry_counter = 1
    queue: list[tuple[int | float, int, NodeType]] = []
    heapq.heappush(queue, (2, 0, start))
    while len(queue) > 0:
        _, _, current = heapq.heappop(queue)
        if current in visited:
            continue
        if done is not None and done(current):
            break
        distance_to_current = distances[current]
        neighbours = [
            neighbour
            for neighbour in get_neighbours(current)
            if neighbour not in visited
        ]
        for neighbour in neighbours:
            distance_current_to_neighbour = get_point_distance(
                current, neighbour)
            new_distance_to_neighbour = (
                distance_to_current + distance_current_to_neighbour
            )
            heapq.heappush(
                queue,
                (new_distance_to_neighbour,
                 queue_entry_counter,
                 neighbour))
            queue_entry_counter += 1
            if new_distance_to_neighbour < distances[neighbour]:
                distances[neighbour] = new_distance_to_neighbour
        visited[current] = True
    return distances


def length_matching_prefixes(one: str, other: str) -> int:
    """
    >>> length_matching_prefixes("hallo", "hamburg")
    2
    >>> length_matching_prefixes("hey", "you")
    0
    >>> length_matching_prefixes("hallo", "hallodu")
    5
    """
    for i in range(min(len(one), len(other))):
        if one[i] != other[i]:
            return i
    return i + 1


def do_part_two(lines: list[str]) -> int:
    orig = lines.pop()
    lines.pop()
    print(f"orig in part two: `{orig}`")
    transforms = [parse_transform(line) for line in lines]

    def count_existing_non_matching_symbols(neighbour: str) -> float:
        prefix_length = length_matching_prefixes(neighbour, orig)
        capitals_in_unmatched_orig = get_number_of_capitals(
            orig[prefix_length:])
        # if summed < 100:
        # return summed // max(len(target) for source, target in transforms)
        return capitals_in_unmatched_orig * 0.9

    def done_func(current: str) -> bool:
        return current.startswith(orig)
        # return current == orig

    def better_neighbours_func(current: str) -> list[str]:
        prefix_length = length_matching_prefixes(current, orig)
        last_rn_in_matching_part = re.search(
            "Rn[^R]*$", current[:prefix_length])
        if last_rn_in_matching_part is None:
            transform_from = 0
        else:
            transform_from = last_rn_in_matching_part.start() + 2
        rest_transformations = get_all_transforms(
            current[transform_from:], transforms)
        return [current[:transform_from] +
                rest for rest in rest_transformations]

    def neighbours_func(current: str) -> list[str]:
        return list(get_all_transforms(current, transforms))

    def distance_func(_: str, __: str) -> int:
        return 1

    start = "e"
    distances_from_e = astar(
        start,
        better_neighbours_func,
        distance_func,
        count_existing_non_matching_symbols,
        done_func,
        orig,
    )

    distance_to_goal = distances_from_e[orig]
    assert isinstance(distance_to_goal, int)
    return distance_to_goal


def do_ex1(lines: list[str]) -> None:
    graph: dict[str, list[tuple[str, int]]] = {}
    for line in lines:
        src, target, dist_str = line.split(" ")
        if src not in graph:
            graph[src] = []
        graph[src].append((target, int(dist_str)))
    print(graph)

    def get_get_neighbours_func(
        graph: dict[str, list[tuple[str, int]]]
    ) -> Callable[[str], list[str]]:
        def get_neighbours(source: str) -> list[str]:
            nonlocal graph
            if source not in graph:
                raise RuntimeError(f"source node {source} not found in graph!")
            return [target for target, _ in graph[source]]

        return get_neighbours

    def get_cost_func(
        graph: dict[str, list[tuple[str, int]]]
    ) -> Callable[[str, str], int | float]:
        def cost_func(source: str, target: str) -> int | float:
            nonlocal graph
            if source not in graph:
                raise RuntimeError(f"source node {source} not found in graph!")
            costs = [
                cost for neighbour,
                cost in graph[source] if neighbour == target]
            if len(costs) != 1:
                raise RuntimeError(
                    f"got {len(costs)} costs from {source} to {target}!")
            return costs[0]

        return cost_func

    distances = dijkstra(
        "A",
        get_get_neighbours_func(graph),
        get_cost_func(graph))
    for node, dist in distances.items():
        print(f"dist from A to {node}: {dist}")


def main(input_file: str) -> None:
    print(f"running for {input_file}")
    # print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    # print()
    # print("part2")
    # if not input_file == "test-input":
    #     result_part_two = do_part_two(lines)
    #     print(result_part_two)
    #     print("done with part two")
    # do_ex1(lines)
    euler_81(lines)


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("done with doctests!")

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
