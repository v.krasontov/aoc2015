import copy
import doctest
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum
from typing import Any, Callable, Optional, overload

import numpy as np


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


def parseLine(line: str) -> None:
    ...


@dataclass
class Player:
    hitpoints: int
    damage: int = 0
    armor: int = 0


def do_one_turn(attacker: Player, defender: Player) -> None:
    defender.hitpoints -= max(1, attacker.damage - defender.armor)


# The first character at or below 0 hit points loses.
# Damage dealt by an attacker each turn is equal to the attacker's damage
# score minus the defender's armor score.
# An attacker always does at least 1 damage.

# have as much gold as you need
# You have 100 hit points

# You must buy exactly one weapon; no dual-wielding. Armor is optional, but you
# can't use more than one. You can buy 0-2 rings (at most one for each hand).
# You must use any items you buy. The shop only has one of each item, so you
# can't buy, for example, two rings of Damage +3.


def do_we_win(us: Player, pc: Player) -> bool:
    while True:
        do_one_turn(us, pc)
        if us.hitpoints <= 0:
            return False
        if pc.hitpoints <= 0:
            return True
        do_one_turn(pc, us)
        if us.hitpoints <= 0:
            return False
        if pc.hitpoints <= 0:
            return True


@dataclass
class Item:
    dmg: int
    armor: int
    cost: int

    def __add__(one: "Item", other: "Item") -> "Item":
        return Item(
            one.dmg + other.dmg,
            one.armor + other.armor,
            one.cost + other.cost)


# Weapons:    Cost  Damage  Armor
# Dagger        8     4       0
# Shortsword   10     5       0
# Warhammer    25     6       0
# Longsword    40     7       0
# Greataxe     74     8       0
#
# Armor:      Cost  Damage  Armor
# Leather      13     0       1
# Chainmail    31     0       2
# Splintmail   53     0       3
# Bandedmail   75     0       4
# Platemail   102     0       5
#
# Rings:      Cost  Damage  Armor
# Damage +1    25     1       0
# Damage +2    50     2       0
# Damage +3   100     3       0
# Defense +1   20     0       1
# Defense +2   40     0       2
# Defense +3   80     0       3


def make_player(items: list[Item],
                init_hitpoints: int = 100) -> tuple[Player, int]:
    cost = 0
    player = Player(hitpoints=init_hitpoints)
    for item in items:
        cost += item.cost
        player.damage += item.dmg
        player.armor += item.armor
    return player, cost


def make_pc() -> Player:
    return Player(hitpoints=109, damage=8, armor=2)


def get_all_options() -> list[list[Item]]:
    weapons = [
        Item(4, 0, 8),
        Item(5, 0, 10),
        Item(6, 0, 25),
        Item(7, 0, 40),
        Item(8, 0, 74),
    ]
    armors = [
        Item(0, 0, 0),
        Item(0, 1, 13),
        Item(0, 2, 31),
        Item(0, 3, 53),
        Item(0, 4, 75),
        Item(0, 5, 102),
    ]
    rings = [
        Item(1, 0, 25),
        Item(2, 0, 50),
        Item(3, 0, 100),
        Item(0, 1, 20),
        Item(0, 2, 40),
        Item(0, 3, 80),
    ]
    options_no_ring = [list(combo)
                       for combo in itertools.product(weapons, armors)]
    options_one_ring = [
        list(combo) for combo in itertools.product(weapons, armors, rings)
    ]
    two_rings = [
        one_ring + other_ring
        for other_ring in rings
        for one_ring in rings
        if other_ring != one_ring
    ]
    options_two_rings = [
        list(combo) for combo in itertools.product(weapons, armors, two_rings)
    ]
    all_options = options_no_ring + options_one_ring + options_two_rings
    return all_options


def do_part_one(lines: list[str]) -> int:
    all_options = get_all_options()
    possible_players_and_costs = [make_player(items) for items in all_options]
    winning_cases = [
        (player, cost)
        for player, cost in possible_players_and_costs
        if do_we_win(player, make_pc())
    ]
    return min([cost for _, cost in winning_cases])


def do_part_two(lines: list[str]) -> int:
    all_options = get_all_options()
    possible_players_and_costs = [make_player(items) for items in all_options]
    losing_cases = [
        (player, cost)
        for player, cost in possible_players_and_costs
        if not do_we_win(player, make_pc())
    ]
    return max([cost for _, cost in losing_cases])


def main(input_file: str) -> None:
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    result_part_one = do_part_one(lines)
    print(result_part_one)
    print("done with part one")

    print()
    print("part2")
    result_part_two = do_part_two(lines)
    print(result_part_two)
    print("done with part two")


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("doctests were successful!")

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
