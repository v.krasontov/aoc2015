import copy
import doctest
import functools
import itertools
import math
import os
import re
import string
import sys
from collections import defaultdict, deque
from dataclasses import dataclass, field
from enum import Enum
from typing import Callable, Optional, overload

DEBUG = True


def readFile(filepath: str) -> list[str]:
    with open(file=filepath, mode="r", encoding="utf-8") as filehandler:
        lines = [line.strip() for line in filehandler.readlines()]
    return lines


@dataclass
class State:
    a: int = 0
    b: int = 0
    instructions: list[str] = field(default_factory=lambda: [])
    pointer: int = 0


def handle_next_instruction(state: State) -> bool:
    """handle_next_instruction takes instruction that state pointer points to
    in the instructions list, and performs it, returning True if that
    instruction is not readable / unknown, terminating the program"""
    try:
        instruction = state.instructions[state.pointer]
    except IndexError:
        print("reached end of program, terminating")
        return True
    instruction_parts = instruction.split(" ")
    try:
        assert 2 <= len(instruction_parts) <= 3
        if instruction.startswith("inc"):
            # inc r increments register r, adding 1 to it, then continues
            #   with the next instruction.
            assert len(instruction_parts) == 2
            assert instruction_parts[1] == "a" or instruction_parts[1] == "b"
            if instruction_parts[1] == "a":
                state.a += 1
            else:
                state.b += 1
            state.pointer += 1
            return False
        if instruction.startswith("hlf"):
            # hlf r sets register r to half its current value, then continues
            #   with the next instruction.
            assert len(instruction_parts) == 2
            assert instruction_parts[1] == "a" or instruction_parts[1] == "b"
            if instruction_parts[1] == "a":
                state.a = state.a // 2
            else:
                state.b = state.b // 2
            state.pointer += 1
            return False
        if instruction.startswith("tpl"):
            # tpl r sets register r to triple its current value, then
            #   continues with the next instruction.
            assert len(instruction_parts) == 2
            assert instruction_parts[1] == "a" or instruction_parts[1] == "b"
            if instruction_parts[1] == "a":
                state.a = state.a * 3
            else:
                state.b = state.b * 3
            state.pointer += 1
            return False
        if instruction.startswith("jmp"):
            # jmp offset is a jump; it continues with the instruction
            #   offset away relative to itself.
            assert len(instruction_parts) == 2
            offset = instruction_parts[1]
            try:
                offset_sign, offset_value = offset[0], int(offset[1:])
            except ValueError as e:
                print(f"could not parse offset: {e}, terminating")
            assert offset_sign in ["+", "-"]
            if offset_sign == "+":
                state.pointer += offset_value
            else:
                state.pointer -= offset_value
            return False
        if instruction.startswith("jie"):
            # jie r, offset is like jmp, but only jumps if register r
            #   is even ("jump if even").
            assert len(instruction_parts) == 3
            offset = instruction_parts[2]
            raw_register = instruction_parts[1]
            assert raw_register.endswith(",")
            register = raw_register[:-1]
            assert register in ["a", "b"]
            try:
                offset_sign, offset_value = offset[0], int(offset[1:])
            except ValueError as e:
                print(f"could not parse offset: {e}, terminating")
            assert offset_sign in ["+", "-"]
            if (register == "a" and state.a % 2) or (
                register == "b" and state.b % 2
            ):  # not even
                state.pointer += 1
                return False
            if offset_sign == "+":
                state.pointer += offset_value
            else:
                state.pointer -= offset_value
            return False
        if instruction.startswith("jio"):
            # jio r, offset is like jmp, but only jumps if register r
            #   is 1 ("jump if one", not odd).
            assert len(instruction_parts) == 3
            offset = instruction_parts[2]
            raw_register = instruction_parts[1]
            assert raw_register.endswith(",")
            register = raw_register[:-1]
            assert register in ["a", "b"]
            try:
                offset_sign, offset_value = offset[0], int(offset[1:])
            except ValueError as e:
                print(f"could not parse offset: {e}, terminating")
            assert offset_sign in ["+", "-"]
            if (register == "a" and state.a != 1) or (
                register == "b" and state.b != 1
            ):  # not one
                state.pointer += 1
                return False
            if offset_sign == "+":
                state.pointer += offset_value
            else:
                state.pointer -= offset_value
            return False
    except AssertionError as e:
        print("assertion error: {e}\ncannot parse instruction, terminating")
        return True
    print("instructions did not match expected pattern, terminating")
    return True


def do_part_one(lines: list[str]) -> int:
    state = State(instructions=lines)
    while not handle_next_instruction(state):
        if DEBUG:
            print(state)
        continue
    return state.b


def do_part_two(lines: list[str]) -> int:
    state = State(a=1, instructions=lines)
    while not handle_next_instruction(state):
        if DEBUG:
            print(state)
        continue
    return state.b


def main(input_file: str) -> None:
    print(f"running for {input_file}")
    print("part1")
    if os.path.isfile(input_file):
        lines = readFile(sys.argv[1])
    else:
        lines = []

    result_part_one = do_part_one(lines)
    print(result_part_one)
    print("done with part one")

    print()
    print("part2")
    result_part_two = do_part_two(lines)
    print(result_part_two)
    print("done with part two")


if __name__ == "__main__":
    sys.setrecursionlimit(30000)
    doctest_result = doctest.testmod()
    if doctest_result.failed:
        print("argh")
        sys.exit(1)
    else:
        print("doctests were successful!")

    file_name = sys.argv[1] if len(sys.argv) > 1 else "test-input"

    main(file_name)
